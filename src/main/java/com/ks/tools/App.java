package com.ks.tools;

import oracle.jdbc.pool.OracleDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class App
{
    public static void main(String[] args)
    {
        System.out.println("-------- Oracle JDBC Connection Testing ------");

        try
        {

            Class.forName("oracle.jdbc.driver.OracleDriver");

        }
        catch (ClassNotFoundException e)
        {

            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return;

        }

        System.out.println("Oracle JDBC Driver Registered!");

        Connection connection = null;

        try
        {
            OracldeDataSource ods = new OracleDataSource();
            ods.setTNSEntryName(System.getProperty("TNSName"));
            ods.setUser("/");
            ods.setPassword("");
            ods.setDriverType("oci");
            connection = ods.getConnection();

        }
        catch (SQLException e)
        {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;

        }

        if (connection != null)
        {
            System.out.println("You made it, take control your database now!");
        }
        else
        {
            System.out.println("Failed to make connection!");
        }
    }
}
